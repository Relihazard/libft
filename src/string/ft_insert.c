/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_insert.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/04 23:03:13 by agrossma          #+#    #+#             */
/*   Updated: 2019/11/29 22:11:07 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_insert(char const *s1, char const *s2, size_t pos)
{
	char	*new;

	new = ft_strnew(ft_strlen(s1) + ft_strlen(s2) + 1);
	if (new == NULL)
		return ((char *)s1);
	ft_strncpy(new, s1, pos);
	ft_strcat(new, s2);
	ft_strcat(new, s1 + pos);
	return (new);
}
